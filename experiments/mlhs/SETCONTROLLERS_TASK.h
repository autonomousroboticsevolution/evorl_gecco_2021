#ifndef SETCONTROLLERS_TASK_H
#define SETCONTROLLERS_TASK_H

#include "ARE/EA.h"
#include "ARE/CPPNGenome.h"
#include "simulatedER/Morphology_CPPNMatrix.h"
#include "CPPNIndividual.h"

#include "eigen3/Eigen/Core"

#include "ARE/learning/Novelty.hpp"
// Learning stuff...
#include <limbo/init/lhs.hpp>
#include "ARE/nn2/NN2Settings.hpp"
#include "ARE/Settings.h"
#include "simulatedER/nn2/NN2Individual.hpp"
#include "multineat/Population.h"

namespace are {

class SETCONTROLLERS_TASK : public EA
{
public:
    SETCONTROLLERS_TASK() : EA(){}
    SETCONTROLLERS_TASK(const misc::RandNum::Ptr& randnum,const settings::ParametersMapPtr& param) : EA(randnum,param){}
    ~SETCONTROLLERS_TASK() override {}

    void init() override;
    void initPopulation();
    void epoch() override;
    bool is_finish() override;
    void setObjectives(size_t indIdx, const std::vector<double> &objectives) override;
    void init_next_pop() override;
    bool update(const Environment::Ptr&);

    NEAT::Genome loadInd(short int genomeID);
    std::vector<int> listInds();

private:
    std::unique_ptr<NEAT::Population> morph_population;

    NEAT::Parameters params;

    std::vector<Eigen::VectorXd> archive;

protected:
    NEAT::RNG rng;
    int currentIndIndex;

};

}

#endif //SETCONTROLLERS_TASK_H
