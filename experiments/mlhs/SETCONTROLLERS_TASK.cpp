#include "SETCONTROLLERS_TASK.h"

using namespace are;

void SETCONTROLLERS_TASK::init()
{
    params = NEAT::Parameters();
    /// Set parameters for NEAT
    unsigned int pop_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    params.PopulationSize = pop_size;
    params.DynamicCompatibility = true;
    params.CompatTreshold = 2.0;
    params.YoungAgeTreshold = 15;
    params.SpeciesMaxStagnation = 100;
    params.OldAgeTreshold = 50;
    params.MinSpecies = 5;
    params.MaxSpecies = 10;
    params.RouletteWheelSelection = false;

    params.MutateRemLinkProb = 0.02;
    params.RecurrentProb = 0.0;
    params.OverallMutationRate = 0.15;
    params.MutateAddLinkProb = 0.08;
    params.MutateAddNeuronProb = 0.01;
    params.MutateWeightsProb = 0.90;
    params.MaxWeight = 8.0;
    params.WeightMutationMaxPower = 0.2;
    params.WeightReplacementMaxPower = 1.0;

    params.MutateActivationAProb = 0.0;
    params.ActivationAMutationMaxPower = 0.5;
    params.MinActivationA = 0.05;
    params.MaxActivationA = 6.0;

    params.MutateNeuronActivationTypeProb = 0.03;

    // Crossover
    params.SurvivalRate = 0.01;
    params.CrossoverRate = 0.01;
    params.CrossoverRate = 0.01;
    params.InterspeciesCrossoverRate = 0.01;

    params.ActivationFunction_SignedSigmoid_Prob = 0.0;
    params.ActivationFunction_UnsignedSigmoid_Prob = 0.0;
    params.ActivationFunction_Tanh_Prob = 1.0;
    params.ActivationFunction_TanhCubic_Prob = 0.0;
    params.ActivationFunction_SignedStep_Prob = 1.0;
    params.ActivationFunction_UnsignedStep_Prob = 0.0;
    params.ActivationFunction_SignedGauss_Prob = 1.0;
    params.ActivationFunction_UnsignedGauss_Prob = 0.0;
    params.ActivationFunction_Abs_Prob = 0.0;
    params.ActivationFunction_SignedSine_Prob = 1.0;
    params.ActivationFunction_UnsignedSine_Prob = 0.0;
    params.ActivationFunction_Linear_Prob = 1.0;

    initPopulation();

    Novelty::archive_adding_prob = settings::getParameter<settings::Double>(parameters,"#archiveAddingProbability").value;
    Novelty::novelty_thr = settings::getParameter<settings::Double>(parameters,"#noveltyThreshold").value;
}

void SETCONTROLLERS_TASK::initPopulation()
{
    // Learning stuff...
    const int num_eval = settings::getParameter<settings::Integer>(parameters,"#numberEvaluations").value;
    const int population_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    float max_weight = settings::getParameter<settings::Float>(parameters,"#MaxWeight").value;
    int nbr_weights, nbr_biases;
    int nn_type = settings::getParameter<settings::Integer>(parameters,"#NNType").value;
    const int nb_input = settings::getParameter<settings::Integer>(parameters,"#NbrInputNeurones").value;
    const int nb_hidden = settings::getParameter<settings::Integer>(parameters,"#NbrHiddenNeurones").value;
    const int nb_output = settings::getParameter<settings::Integer>(parameters,"#NbrOutputNeurones").value;
    if(nn_type == settings::nnType::FFNN)
        NN2Control<ffnn_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else if(nn_type == settings::nnType::RNN)
        NN2Control<rnn_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else if(nn_type == settings::nnType::ELMAN)
        NN2Control<elman_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else {
        std::cerr << "unknown type of neural network" << std::endl;
        abort();
    }
    Eigen::MatrixXd init_samples = limbo::tools::random_lhs(nbr_weights + nbr_biases,num_eval);
    std::vector<double> weights(nbr_weights);
    std::vector<double> biases(nbr_biases);

    rng.Seed(randomNum->getSeed());

    // Morphology
    NEAT::Genome morph_genome(0, 5, 10, 6, false, NEAT::SIGNED_SIGMOID, NEAT::SIGNED_SIGMOID, 0, params, 10);
    morph_population = std::make_unique<NEAT::Population>(morph_genome, params, true, 1.0, randomNum->getSeed());
    for (size_t i = 0; i < population_size; i++){ // Body plans
        for(int j = 0; j < num_eval; j++){ // Controllers
            for(int v = 0; v < nbr_weights; v++)
                weights[v] = max_weight*(init_samples(j,v)*2.f - 1.f);

            for(int w = nbr_weights; w < nbr_weights+nbr_biases; w++)
                biases[w-nbr_weights] = max_weight*(init_samples(j,w)*2.f - 1.f);

            NNParamGenome::Ptr ctrl_gen(new NNParamGenome);
            ctrl_gen->set_weights(weights);
            ctrl_gen->set_biases(biases);

            CPPNGenome::Ptr morphgenome(new CPPNGenome(morph_population->AccessGenomeByIndex(i)));
            CPPNIndividual::Ptr ind(new CPPNIndividual(morphgenome,ctrl_gen));

            ind->set_parameters(parameters);
            ind->set_randNum(randomNum);
            population.push_back(ind);
        }
    }
}

void SETCONTROLLERS_TASK::epoch(){
    const int num_eval = settings::getParameter<settings::Integer>(parameters,"#numberEvaluations").value;
    const int population_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    std::vector<Individual::Ptr> pop;
//    double fitness_average = 0;
    double best_fitness = 0;
    double best_idx = 0;
    double current_fit = 0;
    std::vector<double> bodyPlanFitness;

    for (size_t i = 0; i < population_size; i++) { // Body plans
//        fitness_average = 0;
        best_idx = 0;
	best_fitness = 0;
        for(int j = 0; j < num_eval; j++) { // Controllers
	    current_fit = population.at(i * num_eval + j)->getObjectives().back();
	    if(best_fitness < current_fit){
	            best_fitness = current_fit;
	            best_idx = i*num_eval+j;
	    }
	}
        //fitness_average /= num_eval;
        bodyPlanFitness.push_back(best_fitness);
	pop.push_back(population[best_idx]);
    }
    int indCounter = 0; /// \todo EB: There must be a better way to do this!
    for (size_t i = 0; i < population_size; i++) { // Body plans
        morph_population->AccessGenomeByIndex(indCounter).SetFitness(bodyPlanFitness.at(indCounter));
        indCounter++;
    }
    morph_population->Epoch();
    population = pop;
}

void SETCONTROLLERS_TASK::setObjectives(size_t indIdx, const std::vector<double> &objectives){
    currentIndIndex = indIdx;
    population[indIdx]->setObjectives(objectives);
}

void SETCONTROLLERS_TASK::init_next_pop(){
    population.clear();
    // Learning stuff...
    const int num_eval = settings::getParameter<settings::Integer>(parameters,"#numberEvaluations").value;
    const int population_size = settings::getParameter<settings::Integer>(parameters,"#populationSize").value;
    float max_weight = settings::getParameter<settings::Float>(parameters,"#MaxWeight").value;
    int nbr_weights, nbr_biases;
    int nn_type = settings::getParameter<settings::Integer>(parameters,"#NNType").value;
    const int nb_input = settings::getParameter<settings::Integer>(parameters,"#NbrInputNeurones").value;
    const int nb_hidden = settings::getParameter<settings::Integer>(parameters,"#NbrHiddenNeurones").value;
    const int nb_output = settings::getParameter<settings::Integer>(parameters,"#NbrOutputNeurones").value;
    if(nn_type == settings::nnType::FFNN)
        NN2Control<ffnn_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else if(nn_type == settings::nnType::RNN)
        NN2Control<rnn_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else if(nn_type == settings::nnType::ELMAN)
        NN2Control<elman_t>::nbr_parameters(nb_input,nb_hidden,nb_output,nbr_weights,nbr_biases);
    else {
        std::cerr << "unknown type of neural network" << std::endl;
        abort();
    }
    Eigen::MatrixXd init_samples = limbo::tools::random_lhs(nbr_weights + nbr_biases,num_eval);
    std::vector<double> weights(nbr_weights);
    std::vector<double> biases(nbr_biases);

    rng.Seed(randomNum->getSeed());

    for (size_t i = 0; i < population_size; i++) // Body plans
    {
        for(int j = 0; j < num_eval; j++){ // Controllers
            for(int v = 0; v < nbr_weights; v++)
                weights[v] = max_weight*(init_samples(j,v)*2.f - 1.f);

            for(int w = nbr_weights; w < nbr_weights+nbr_biases; w++)
                biases[w-nbr_weights] = max_weight*(init_samples(j,w)*2.f - 1.f);

            NNParamGenome::Ptr ctrl_gen(new NNParamGenome);
            ctrl_gen->set_weights(weights);
            ctrl_gen->set_biases(biases);

            CPPNGenome::Ptr morphgenome(new CPPNGenome(morph_population->AccessGenomeByIndex(i)));
            CPPNIndividual::Ptr ind(new CPPNIndividual(morphgenome,ctrl_gen));

            ind->set_parameters(parameters);
            ind->set_randNum(randomNum);
            population.push_back(ind);
        }
    }
}

bool SETCONTROLLERS_TASK::is_finish()
{
    unsigned int maxGenerations = settings::getParameter<settings::Integer>(parameters,"#numberOfGeneration").value;
    return get_generation() > maxGenerations;
}

NEAT::Genome SETCONTROLLERS_TASK::loadInd(short genomeID)
{
    std::string loadExperiment = settings::getParameter<settings::String>(parameters,"#loadExperiment").value;
    std::cout << "Loading genome: " << genomeID << "!" << std::endl;
    std::stringstream filepath;
    filepath << loadExperiment << "/morphGenome" << genomeID;
    NEAT::Genome indGenome(filepath.str().c_str());
    return indGenome;
}

std::vector<int> SETCONTROLLERS_TASK::listInds()
{
    std::vector<int> robotList;
    // This code snippet was taken from: https://www.gormanalysis.com/blog/reading-and-writing-csv-files-with-cpp/
    std::string loadExperiment = settings::getParameter<settings::String>(parameters,"#loadExperiment").value;
    std::string bootstrapFile = settings::getParameter<settings::String>(parameters,"#bootstrapFile").value;
    // Create an input filestream
    std::ifstream myFile(loadExperiment+bootstrapFile);
    if(!myFile.is_open()) throw std::runtime_error("Could not open file");
    std::string line;
    int val;

    while(std::getline(myFile, line)){
        // Create a stringstream of the current line
        std::stringstream ss(line);
        // Keep track of the current column index
        int colIdx = 0;
        // Extract each integer
        while(ss >> val) {
            // Add the current integer to the 'colIdx' column's values vector
            robotList.push_back(val);
            // If the next token is a comma, ignore it and move on
            if(ss.peek() == ',') ss.ignore();
            // Increment the column index
            colIdx++;
        }
    }
    // Close file
    myFile.close();
    return robotList;
}

bool SETCONTROLLERS_TASK::update(const Environment::Ptr& env)
{
//    for(const auto& ind : population) {
//        std::dynamic_pointer_cast<NN2Individual>(ind)->set_final_position(
//                std::dynamic_pointer_cast<MazeEnv>(env)->get_final_position());
//    }
//    endEvalTime = hr_clock::now();
//    numberEvaluation++;
//
    if(simulator_side){
        Individual::Ptr ind = population[currentIndIndex];
        std::dynamic_pointer_cast<sim::NN2Individual>(ind)->set_final_position(env->get_final_position());
        std::dynamic_pointer_cast<sim::NN2Individual>(ind)->set_trajectory(env->get_trajectory());
    }
    return true;
}
