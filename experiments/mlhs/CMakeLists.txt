cmake_minimum_required(VERSION 3.0)

set(INCLUDES 
    $<$<BOOL:${VREP_FOUND}>:${VREP_FOLDER}/programming/include> 
    $<$<BOOL:${COPPELIASIM_FOUND}>:${COPPELIASIM_FOLDER}/programming/include> 
    "../../simulation/include" 
    "../../EAFramework/include" 
    "../common/" 
    "/usr/include/eigen3" 
    "../../modules/") 
 
SET(VREP_SRC 
    $<$<BOOL:${VREP_FOUND}>:${VREP_FOLDER}/programming/common/v_repLib.cpp> 
    $<$<BOOL:${VREP_FOUND}>:${VREP_FOLDER}/programming/remoteApi/extApi.c> 
    $<$<BOOL:${VREP_FOUND}>:${VREP_FOLDER}/programming/remoteApi/extApiPlatform.c> 
    $<$<BOOL:${VREP_FOUND}>:${VREP_FOLDER}/programming/common/shared_memory.c> 
    $<$<BOOL:${COPPELIASIM_FOUND}>:${COPPELIASIM_FOLDER}/programming/common/simLib.cpp> 
    $<$<BOOL:${COPPELIASIM_FOUND}>:${COPPELIASIM_FOLDER}/programming/remoteApi/extApi.c> 
    $<$<BOOL:${COPPELIASIM_FOUND}>:${COPPELIASIM_FOLDER}/programming/remoteApi/extApiPlatform.c> 
    $<$<BOOL:${COPPELIASIM_FOUND}>:${COPPELIASIM_FOLDER}/programming/common/shared_memory.c> 
    ) 



add_library(setcontrollers_task SHARED
    SETCONTROLLERS_TASK.cpp
    CPPNIndividual.cpp
    factories.cpp
    ManLog.cpp)
target_include_directories(setcontrollers_task PUBLIC ${INCLUDES})
target_link_libraries(setcontrollers_task ARE simulatedER)
install(TARGETS setcontrollers_task DESTINATION lib)
